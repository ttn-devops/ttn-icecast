#!/bin/sh
sed -i 's/SOURCE_COUNT/'"${SOURCE_COUNT}"'/' /etc/icecast.xml
sed -i 's/SOURCE_PASSWORD/'"${SOURCE_PASSWORD}"'/' /etc/icecast.xml
sed -i 's/RELAY_PASSWORD/'"${RELAY_PASSWORD}"'/' /etc/icecast.xml
sed -i 's/ADMIN_USER/'"${ADMIN_USER}"'/' /etc/icecast.xml
sed -i 's/ADMIN_PASSWORD/'"${ADMIN_PASSWORD}"'/' /etc/icecast.xml


exec icecast -c /etc/icecast.xml
