FROM alpine:latest

RUN apk -U add icecast

COPY json.xsl /usr/share/icecast2/web/json.xsl
COPY icecast.xml /etc/icecast.xml
COPY docker-entry-point.sh /usr/local/bin/

EXPOSE 8000

CMD ["docker-entry-point.sh"]
